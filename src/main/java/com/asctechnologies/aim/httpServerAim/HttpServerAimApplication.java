package com.asctechnologies.aim.httpServerAim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpServerAimApplication {

	public static void main(String[] args) {
		SpringApplication.run(HttpServerAimApplication.class, args);
	}

}
